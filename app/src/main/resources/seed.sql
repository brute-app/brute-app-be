INSERT INTO genders (genders.id, genders.name)
VALUES (1, 'Male'),
       (2, 'Female');

INSERT INTO roles (roles.id, roles.name)
VALUES (1, 'User'),
       (2, 'Admin');

INSERT INTO users (users.id, users.email, users.full_name, users.gender_id, users.password, users.deleted)
VALUES (1, 'admin@brute.com', 'ADMIN', 1, '$2a$10$WBPUGlVt55hIK6BVyJWt6eX3dvV/MaGqqindXD2iNMaNXXGJsp7Dm', 0);

INSERT INTO roles_users (roles_users.roles_id, roles_users.users_id)
VALUES (1, 2);

INSERT INTO user_transactions (user_transactions.id, user_transactions.user_id, user_transactions.description, user_transactions.points)
VALUES (1, 1, 'have fun', 99999);

