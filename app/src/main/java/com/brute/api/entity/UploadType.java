package com.brute.api.entity;

import javax.persistence.*;
import lombok.*;

@NoArgsConstructor
@Entity
@Table(name = "upload_types")
public class UploadType extends BaseEntity {

    private String name;

    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
