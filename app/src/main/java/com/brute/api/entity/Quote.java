package com.brute.api.entity;

import java.sql.Date;
import javax.persistence.*;

@Entity
@Table(name = "quotes")
public class Quote extends BaseEntity {

    private String quote;
    private String author;
    private Date dateShown;

    public String getQuote() {
        return quote;
    }

    public void setQuote(String quote) {
        this.quote = quote;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Date getDateShown() {
        return dateShown;
    }

    public void setDateShown(Date dateShown) {
        this.dateShown = dateShown;
    }
}
