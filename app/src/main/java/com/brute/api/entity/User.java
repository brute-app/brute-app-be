package com.brute.api.entity;

import javax.persistence.*;
import java.util.*;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

@Entity
@Table(name = "users")
public class User extends BaseEntity implements UserDetails {

    private String email;

    private String username;

    private String password;

    private String fullName;

    private String phoneNumber;

    private String referralToken;

    private Boolean isOnline;

    private Boolean isDeleted;

    private Date registeredOn;

    private Date updatedOn;

    private Gender gender;

    private Upload profileImg;

    private Set<Upload> uploads;

    private Set<Role> roles;

    private Set<Conversation> conversations;

    private List<Transaction> transactions;

    public User() {
        this.conversations = new HashSet<>();
        this.uploads = new HashSet<>();
        this.roles = new HashSet<>();
    }

    @Override
    @Transient
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return this.roles;
    }

    @Override
    @Transient
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    @Transient
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    @Transient
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    @Transient
    public boolean isEnabled() {
        return true;
    }

    public void addRole(Role role) {
        this.roles.add(role);
        role.addUser(this);
    }

    @Column(name = "email", unique = true)
    public String getEmail() {
        return email;
    }

    @Override
    @Column(name = "username", unique = true)
    public String getUsername() {
        return username;
    }

    @Override
    @Column(name = "password", unique = true)
    public String getPassword() {
        return password;
    }

    @Column(name = "full_name")
    public String getFullName() {
        return fullName;
    }

    @Column(name = "phone_number")
    public String getPhoneNumber() {
        return phoneNumber;
    }

    @Column(name = "online")
    public Boolean isOnline() {
        return isOnline;
    }

    @Column(name = "deleted")
    public Boolean isDeleted() {
        return isDeleted;
    }

    @Column(name = "registered_on")
    public Date getRegisteredOn() {
        return registeredOn;
    }

    @Column(name = "updated_on")
    public Date getUpdatedOn() {
        return updatedOn;
    }

    @Column(name = "referal_token")
    public String getReferralToken() {
        return referralToken;
    }

    @OneToOne(targetEntity = Upload.class)
    public Upload getProfileImg() {
        return profileImg;
    }

    @OneToMany(targetEntity = Upload.class, mappedBy = "user", fetch = FetchType.EAGER)
    public Set<Upload> getUploads() {
        return uploads;
    }

    @ManyToMany(targetEntity = Role.class, mappedBy = "users", fetch = FetchType.EAGER)
    public Set<Role> getRoles() {
        return roles;
    }

    @ManyToMany(fetch = FetchType.EAGER)
    public Set<Conversation> getConversations() {
        return this.conversations;
    }

    @OneToMany(targetEntity = Transaction.class, mappedBy = "user", fetch = FetchType.EAGER)
    public List<Transaction> getTransactions() {
        return transactions;
    }

    @ManyToOne
    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void setOnline(Boolean online) {
        isOnline = online;
    }

    public void setDeleted(Boolean deleted) {
        isDeleted = deleted;
    }

    public void setRegisteredOn(Date registeredOn) {
        this.registeredOn = registeredOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    public void setProfileImg(Upload profileImg) {
        this.profileImg = profileImg;
    }

    public void setUploads(Set<Upload> uploads) {
        this.uploads = uploads;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public void setConversations(Set<Conversation> conversations) {
        this.conversations = conversations;
    }

    public void setReferralToken(String referalToken) {
        this.referralToken = referalToken;
    }

    public void setTransactions(List<Transaction> transactions) {
        this.transactions = transactions;
    }

    @Transient
    public boolean addConversation(Conversation conversation) {
        return this.conversations.add(conversation);
    }

}
