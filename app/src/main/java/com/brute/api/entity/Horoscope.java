package com.brute.api.entity;

import java.util.Date;
import javax.persistence.*;

@Entity
@Table(name = "horoscopes")
public class Horoscope extends BaseEntity {

    @Enumerated(EnumType.STRING)
    @Column(name = "zodiac_sign")
    private ZodiacSign sign;
    @Column(name = "info")
    private String info;
    @Column(name = "period_from")
    private Date periodFrom;
    @Column(name = "period_to")
    private Date periodTo;

    public ZodiacSign getSign() {
        return sign;
    }

    public void setSign(ZodiacSign sign) {
        this.sign = sign;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public Date getPeriodFrom() {
        return periodFrom;
    }

    public void setPeriodFrom(Date from) {
        this.periodFrom = from;
    }

    public Date getPeriodTo() {
        return periodTo;
    }

    public void setPeriodTo(Date to) {
        this.periodTo = to;
    }
}
