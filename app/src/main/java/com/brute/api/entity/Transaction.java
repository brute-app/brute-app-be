package com.brute.api.entity;

import javax.persistence.*;

@Entity
@Table(name = "user_transactions")
public class Transaction extends BaseEntity {

    private User user;
    private long points;
    private String description;

    @ManyToOne
    public User getUser() {
        return user;
    }

    @Column(name = "points")
    public long getPoints() {
        return points;
    }

    @Column(name = "description")
    public String getDescription() {
        return description;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setPoints(long points) {
        this.points = points;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
