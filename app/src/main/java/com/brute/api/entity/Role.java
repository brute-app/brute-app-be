package com.brute.api.entity;

import java.util.*;
import javax.persistence.*;
import org.springframework.security.core.GrantedAuthority;

@Entity
@Table(name = "roles")
public class Role extends BaseEntity implements GrantedAuthority {

    private String name;

    private Set<User> users;

    public Role() {
        this.users = new HashSet<>();
    }

    @ManyToMany
    public Set<User> getUsers() {
        return users;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }

    @Override
    @Transient
    public String getAuthority() {
        return this.name;
    }

    public void addUser(User user) {
        this.users.add(user);
    }

    @Column(name = "name")
    public String getName() {
        return name;
    }
}
