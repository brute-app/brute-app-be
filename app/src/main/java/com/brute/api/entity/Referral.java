package com.brute.api.entity;

import javax.persistence.*;

@Entity
@Table(name = "user_referrals")
public class Referral extends BaseEntity {

    private long userId;
    private long referralUser;

    @Column(name = "user_id")
    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    @Column(name = "referral_user")
    public long getReferralUser() {
        return referralUser;
    }

    public void setReferralUser(long referralUser) {
        this.referralUser = referralUser;
    }
}
