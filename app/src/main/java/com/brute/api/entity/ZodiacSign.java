package com.brute.api.entity;

import java.util.Arrays;

public enum ZodiacSign {
    AQUARIUS("Водолей"),
    PISCES("Риби"),
    ARIES("Овен"),
    TAURUS("Телец"),
    GEMINI("Близнаци"),
    CANCER("Рак"),
    LEO("Лъв"),
    VIRGO("Дева"),
    LIBRA("Везни"),
    SCORPIO("Скорпион"),
    SAGITTARIUS("Стрелец"),
    CAPRICORN("Козерог"),
    BRUTE("Брут");

    private String label;

    ZodiacSign(String label) {
        this.label = label;
    }

    public static ZodiacSign from(final String text) {
        return Arrays.stream(ZodiacSign.values())
            .filter(z -> z.label.equalsIgnoreCase(text) || z.name().equalsIgnoreCase(text))
            .findFirst()
            .orElse(ZodiacSign.BRUTE);
    }
}
