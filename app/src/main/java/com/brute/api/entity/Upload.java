package com.brute.api.entity;

import javax.persistence.*;

@Entity
@Table(name = "uploads")
public class Upload extends BaseEntity {

    private String url;

    private String thumbnailUrl;

    private User user;

    private Long typeId;

    private Boolean video;

    public Upload() {
    }

    @Column(name = "url")
    public String getUrl() {
        return url;
    }

    @Column(name = "thumbnail_url")
    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    @ManyToOne
    public User getUser() {
        return user;
    }

    @Column(name = "fk_type_id")
    public Long getTypeId() {
        return typeId;
    }

    @Column(name = "video")
    public Boolean getVideo() {
        return video;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setTypeId(Long typeId) {
        this.typeId = typeId;
    }

    public void setVideo(Boolean video) {
        this.video = video;
    }
}
