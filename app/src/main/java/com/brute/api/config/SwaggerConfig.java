package com.brute.api.config;

import static springfox.documentation.builders.PathSelectors.regex;
import java.util.Collections;
import java.util.List;

import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
@Import(BeanValidatorPluginsConfiguration.class)
public class SwaggerConfig {

    public static final String AUTHORIZATION_HEADER = "Authorization";
    public static final String JWT_TOKEN = "jwtToken";
    public static final String HEADER = "header";
    public static final String DEFAULT_INCLUDE_PATTERN = "/api/.*";

    @Value("${app.version}")
    private String appVersion;

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
            .select()
//            .paths(regex(DEFAULT_INCLUDE_PATTERN))
            .apis(RequestHandlerSelectors.any())
            .paths(PathSelectors.any())
            .build()
            .genericModelSubstitutes(ResponseEntity.class)
            .ignoredParameterTypes(Pageable.class)
            // Adding Application base descriptions
            .apiInfo(apiInfo())
            // remove default swagger responses
            .useDefaultResponseMessages(false)
            .securityContexts(Lists.newArrayList(securityContext()))
            .securitySchemes(Collections.singletonList(apiKey()));
    }

    private SecurityContext securityContext() {
        return SecurityContext.builder()
            .securityReferences(defaultAuth())
            .forPaths(regex(DEFAULT_INCLUDE_PATTERN))
            .build();
    }

    List<SecurityReference> defaultAuth() {
        AuthorizationScope authorizationScope
            = new AuthorizationScope("global", "accessEverything");
        AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
        authorizationScopes[0] = authorizationScope;
        return Lists.newArrayList(
            new SecurityReference("JWT", authorizationScopes));
    }

    private ApiKey apiKey() {
        return new ApiKey(JWT_TOKEN, AUTHORIZATION_HEADER, HEADER);
    }

    private ApiInfo apiInfo() {
        return new ApiInfo(
            "Brute API",
            "Brute offers and more.",
            appVersion,
            "Terms of service pending.",
            new Contact("Brute Ltd.", "http://www.brute.com/", "admin@brute.com"),
            "All Rights Reserved, Copyrights © 2021, Brute",
            null,
            Collections.emptyList());
    }

}