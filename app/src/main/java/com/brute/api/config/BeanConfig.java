package com.brute.api.config;

import static com.brute.api.constant.ConfigurationConstants.*;

import freemarker.cache.ClassTemplateLoader;
import freemarker.template.*;
import java.io.IOException;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.*;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
public class BeanConfig {

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public freemarker.template.Configuration freemarkerConfiguration() throws IOException {
        var cfg = new freemarker.template.Configuration(new Version(CONFIG_VERSION));

        cfg.setTemplateLoader(new ClassTemplateLoader(this.getClass(), TEMPLATE_BASE));
        cfg.setDefaultEncoding(UTF);
        cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);

        return cfg;
    }
}
