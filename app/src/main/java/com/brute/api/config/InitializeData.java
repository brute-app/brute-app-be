package com.brute.api.config;

import javax.sql.DataSource;

import com.brute.api.repository.GenderRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class InitializeData {

    private final DataSource dataSource;
    private final GenderRepository genderRepository;

    @EventListener(ApplicationReadyEvent.class)
    public void loadData() {
        if (genderRepository.findAll().size() > 0) {
            return;
        }
        ResourceDatabasePopulator resourceDatabasePopulator = new ResourceDatabasePopulator(
            false,
            false,
            "UTF-8",
            new ClassPathResource("seed.sql"));
        resourceDatabasePopulator.execute(dataSource);
    }
}
