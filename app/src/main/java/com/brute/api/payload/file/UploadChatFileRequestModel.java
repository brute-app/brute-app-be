package com.brute.api.payload.file;

import org.springframework.web.multipart.MultipartFile;

/**
 * @author ITenev
 * created on 2/28/2020
 */
public class UploadChatFileRequestModel {

    private MultipartFile file;

    public MultipartFile getFile() {
        return this.file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }

}
