package com.brute.api.payload.horoscope;

import com.brute.api.entity.*;
import java.util.Date;
import lombok.*;

@Data
@Builder
public class HoroscopeModel {

    private Long id;
    private ZodiacSign sign;
    private String info;
    private Date from;
    private Date to;

    public static HoroscopeModel ofHoroscope(Horoscope h) {
        if(h == null) {
            return HoroscopeModel.builder().build();
        }
        return HoroscopeModel.builder()
            .id(h.getId())
            .sign(h.getSign())
            .info(h.getInfo())
            .from(h.getPeriodFrom())
            .to(h.getPeriodTo())
            .build();
    }

}
