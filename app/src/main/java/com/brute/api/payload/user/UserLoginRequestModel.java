package com.brute.api.payload.user;

import lombok.Data;

@Data
public class UserLoginRequestModel {

    private String username;

    private String password;
}
