package com.brute.api.payload.user;

import com.brute.api.entity.Upload;
import java.util.List;
import java.util.stream.Collectors;
import lombok.*;
import org.springframework.data.domain.Page;

@Getter
@Setter
public class UserUploadsViewModel {

    private Integer pageNumber;

    private Integer pageSize;

    private Integer totalPages;

    private Long totalElements;

    private List<UserImagesViewModel> uploads;

    public static UserUploadsViewModel ofUserUploads(Page<Upload> u){
        var model = new UserUploadsViewModel();
        model.setPageNumber(u.getNumber());
        model.setPageSize(u.getSize());
        model.setTotalPages(u.getTotalPages());
        model.setTotalElements(u.getTotalElements());
        model.setUploads(u.getContent().stream().map(UserImagesViewModel::ofUserImages).collect(Collectors.toList()));

        return model;
    }
}
