package com.brute.api.payload.user;

public class EmailRequestModel {

    private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
