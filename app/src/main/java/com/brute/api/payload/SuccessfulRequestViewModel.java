package com.brute.api.payload;

import lombok.*;

@Data
@AllArgsConstructor
@Builder
public class SuccessfulRequestViewModel {

    private boolean isSuccessful;
    private String message;

}
