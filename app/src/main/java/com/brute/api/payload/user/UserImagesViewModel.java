package com.brute.api.payload.user;

import com.brute.api.entity.Upload;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
public class UserImagesViewModel {

    private Long id;

    private String fullUrl;

    private String thumbnailUrl;

    private Long typeId;

    public static UserImagesViewModel ofUserImages(Upload upload) {
        return new UserImagesViewModel(
            upload.getId(),
            upload.getUrl(),
            upload.getThumbnailUrl(),
            upload.getTypeId()
        );
    }
}
