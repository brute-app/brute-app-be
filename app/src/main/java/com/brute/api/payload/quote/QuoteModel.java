package com.brute.api.payload.quote;

import com.brute.api.entity.Quote;
import java.sql.Date;
import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class QuoteModel {

    private Long id;
    private String quote;
    private String author;
    private Date dateShown;

    public static QuoteModel ofQuote(Quote q) {
        return QuoteModel.builder()
            .id(q.getId())
            .quote(q.getQuote())
            .author(q.getAuthor())
            .dateShown(q.getDateShown())
            .build();
    }
}
