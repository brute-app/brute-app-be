package com.brute.api.payload.chat;


public class MessageRequestModel {

    private String message;

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
