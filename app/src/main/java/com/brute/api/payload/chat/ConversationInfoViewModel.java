package com.brute.api.payload.chat;

import com.brute.api.entity.Conversation;
import java.util.List;
import java.util.stream.Collectors;
import lombok.*;

@Data
public class ConversationInfoViewModel {

    private String conversationName;

    private Long conversationId;

    private MessageInfoViewModel lastMessage;

    private List<UserConversationInfoViewModel> users;

    private ConversationInfoViewModel(
            String name,
            Long conversationId,
            List<UserConversationInfoViewModel> users
    ) {
        this.conversationId = conversationId;
        this.users = users;
    }

    private ConversationInfoViewModel(
            String name,
            Long conversationId,
            List<UserConversationInfoViewModel> users,
            MessageInfoViewModel lastMessage
            ) {
        this(name, conversationId, users);
        this.lastMessage = lastMessage;
    }

    public static ConversationInfoViewModel ofConversation(Conversation conversation) {
        final var lastMessage = conversation.getLastMessage();
        if (lastMessage == null)
            return new ConversationInfoViewModel(
                    conversation.getName(),
                    conversation.getId(),
                    conversation.getUsers().stream()
                            .map(UserConversationInfoViewModel::ofUserConversation)
                            .collect(Collectors.toList())
            );

        return new ConversationInfoViewModel(
                conversation.getName(),
                conversation.getId(),
                conversation.getUsers().stream()
                .map(UserConversationInfoViewModel::ofUserConversation)
                .collect(Collectors.toList()),
                MessageInfoViewModel.ofMessage(lastMessage)
                );
    }

}
