package com.brute.api.payload.user;

/**
 * @author ITenev
 * created on 2/5/2020
 */
public class UserProfileUploadRequestModel {

    private Long uploadId;

    public Long getUploadId() {
        return this.uploadId;
    }

    public void setUploadId(Long uploadId) {
        this.uploadId = uploadId;
    }

}
