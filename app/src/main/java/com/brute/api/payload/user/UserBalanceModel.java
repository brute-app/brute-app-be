package com.brute.api.payload.user;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UserBalanceModel {

    private Long balance;
}
