package com.brute.api.payload.gender;

import com.brute.api.entity.Gender;
import lombok.*;

@Data
@AllArgsConstructor
public class GenderInfoViewModel {

    private Long id;
    private String name;

    public static GenderInfoViewModel ofGender(Gender gender) {
        return new GenderInfoViewModel(gender.getId(), gender.getName());
    }
}
