package com.brute.api.payload.user;

import com.brute.api.entity.*;
import java.util.*;
import java.util.stream.Collectors;
import lombok.*;

@Getter
@Setter
public class UserInfoViewModel {

    private Long id;

    private String email;

    private String username;

    private String fullName;

    private String phoneNumber;

    private String referralToken;

    private Long balance;

    private boolean isOnline;

    private boolean isDeleted;

    private Date registeredOn;

    private Date updatedOn;

    private UserImagesViewModel profileImg;

    private Set<UserImagesViewModel> uploads;

    private Set<String> roles;

    private String gender;

    public static UserInfoViewModel fromUser(User u) {
        var model = new UserInfoViewModel();

        model.setId(u.getId());
        model.setEmail(u.getEmail());
        model.setUsername(u.getUsername());
        model.setPhoneNumber(u.getPhoneNumber());
        model.setReferralToken(u.getReferralToken());
        model.setUploads(u.getUploads().stream().map(UserImagesViewModel::ofUserImages).collect(Collectors.toSet()));
        model.setRoles(u.getRoles().stream().map(Role::getName).collect(Collectors.toSet()));
        model.setGender(u.getGender().getName());

        if (u.getProfileImg() != null) {
            model.setProfileImg(UserImagesViewModel.ofUserImages(u.getProfileImg()));
        }

        return model;
    }
}
