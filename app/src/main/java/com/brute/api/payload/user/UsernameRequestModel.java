package com.brute.api.payload.user;

/**
 * @author ITenev
 * created on 3/17/2020
 */
public class UsernameRequestModel {

    private String username;

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
