package com.brute.api.payload.horoscope;

import com.brute.api.entity.ZodiacSign;
import java.sql.Date;
import lombok.*;

@Data
@Builder
public class HoroscopeRequestModel {

    private Long id;
    private ZodiacSign sign;
    private String info;
    private Date from;
    private Date to;
}
