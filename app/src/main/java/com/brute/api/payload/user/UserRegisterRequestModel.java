package com.brute.api.payload.user;

import com.brute.api.constant.ConfigurationConstants;
import javax.validation.constraints.*;
import lombok.*;

@Getter
@Setter
public class UserRegisterRequestModel {

    @Email(message = ConfigurationConstants.User.ERROR_EMAIL_VALIDATION)
    private String email;

    @Pattern(regexp = ".*", message = ConfigurationConstants.User.ERROR_PASSWORD_VALIDATION)
    private String password;

    private String username;

    private String confirm;

    private Long genderId;

    private String phoneNumber;

    private String fullName;

    private String referralCode;
}
