package com.brute.api.payload.user;

/**
 * @author ITenev
 * created on 1/29/2020
 */
public class UsernameValidateInfoModel {

    private boolean email;

    private boolean username;

    public UsernameValidateInfoModel(boolean email, boolean username) {
        this.email = email;
        this.username = username;
    }

    public boolean isEmail() {
        return this.email;
    }

    public boolean isUsername() {
        return this.username;
    }

}
