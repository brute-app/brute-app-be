package com.brute.api.payload.quote;

import java.sql.Date;
import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class QuoteRequestModel {

    private Long id;
    private String quote;
    private String author;
    private Date dateShown;
}
