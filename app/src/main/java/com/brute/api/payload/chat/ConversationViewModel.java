package com.brute.api.payload.chat;

import com.brute.api.entity.*;
import java.util.*;
import java.util.stream.Collectors;
import lombok.Data;
import org.springframework.data.domain.Page;

@Data
public class ConversationViewModel {

    private String name;

    private Date startedAt;

    private List<MessageInfoViewModel> messages;

    private List<UserConversationInfoViewModel> users;

    private Integer pageNumber;

    private Integer pageSize;

    private Integer totalPages;

    private Long totalElements;

    public ConversationViewModel(String name, Date startedAt, List<MessageInfoViewModel> messages, List<UserConversationInfoViewModel> users,
        Integer pageNumber, Integer pageSize, Integer totalPages, Long totalElements) {
        this.name = name;
        this.startedAt = startedAt;
        this.messages = messages;
        this.users = users;
        this.pageNumber = pageNumber;
        this.pageSize = pageSize;
        this.totalPages = totalPages;
        this.totalElements = totalElements;
    }

    public static ConversationViewModel ofConversation(Conversation conversation, Page<Message> messages) {
        final var content = messages.map(MessageInfoViewModel::ofMessage).getContent();
        return new ConversationViewModel(
            conversation.getName(),
            conversation.getStartedAt(),
            content,
            conversation.getUsers().stream().map(UserConversationInfoViewModel::ofUserConversation).collect(Collectors.toList()),
            messages.getNumber(),
            messages.getSize(),
            messages.getTotalPages(),
            messages.getTotalElements()
        );
    }
}
