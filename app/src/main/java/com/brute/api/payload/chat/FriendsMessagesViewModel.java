package com.brute.api.payload.chat;

import com.brute.api.entity.Conversation;
import java.util.*;
import java.util.stream.Collectors;
import org.springframework.data.domain.Page;

public class FriendsMessagesViewModel {

    private Integer pageNumber;

    private Integer pageSize;

    private Integer totalPages;

    private Long totalElements;

    private List<ConversationsDto> conversations;

    public FriendsMessagesViewModel() {
        this.conversations = new ArrayList<>();
    }

    public static FriendsMessagesViewModel ofFriendsMessages(Page<Conversation> p) {
        var model = new FriendsMessagesViewModel();
        model.setPageNumber(p.getNumber());
        model.setPageSize(p.getSize());
        model.setTotalPages(p.getTotalPages());
        model.setTotalElements(p.getTotalElements());
        model.setConversations(p.getContent().stream().map(ConversationsDto::new).collect(Collectors.toList()));

        return model;
    }

    public Integer getPageNumber() {
        return this.pageNumber;
    }

    public void setPageNumber(Integer pageNumber) {
        this.pageNumber = pageNumber;
    }

    public Integer getPageSize() {
        return this.pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Integer getTotalPages() {
        return this.totalPages;
    }

    public void setTotalPages(Integer totalPages) {
        this.totalPages = totalPages;
    }

    public Long getTotalElements() {
        return this.totalElements;
    }

    public void setTotalElements(Long totalElements) {
        this.totalElements = totalElements;
    }

    public List<ConversationsDto> getConversations() {
        return conversations;
    }

    public void setConversations(List<ConversationsDto> conversations) {
        this.conversations = conversations;
    }
}
