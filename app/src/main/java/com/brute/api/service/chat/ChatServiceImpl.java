package com.brute.api.service.chat;

import com.brute.api.entity.*;
import com.brute.api.exception.user.*;
import com.brute.api.payload.SuccessfulRequestViewModel;
import com.brute.api.payload.chat.*;
import com.brute.api.repository.*;
import com.brute.api.service.common.io.PictureService;
import java.time.*;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;

@Service
public class ChatServiceImpl implements ChatService {

    private final ConversationRepository conversationRepository;

    private final UserRepository userRepository;

    private final MessageRepository messageRepository;

    private final PictureService pictureService;


    public ChatServiceImpl(
        ConversationRepository conversationRepository,
        UserRepository userRepository,
        MessageRepository messageRepository,
        PictureService pictureService
    ) {
        this.conversationRepository = conversationRepository;
        this.userRepository = userRepository;
        this.messageRepository = messageRepository;
        this.pictureService = pictureService;
    }

    @Override
    public FriendsMessagesViewModel getAllConversationsWithLastMessageByUserId(Long id, Integer page, Integer size) {
        final var pageable = PageRequest.of(page, size);

        return FriendsMessagesViewModel.ofFriendsMessages(
            this.conversationRepository.findAllConversationsWithLastMessageByUserId(id, pageable)
        );
    }

    @Override
    public ConversationInfoViewModel createConversation(ConversationRequestModel model) {
        final var conversation = new Conversation();
        conversation.setStartedAt(model.getStartedAt());
        conversation.setUsers(this.userRepository.findAllById(model.getUsersId()));

        return ConversationInfoViewModel.ofConversation(
            this.conversationRepository.saveAndFlush(
                conversation
            )
        );
    }

    @Override
    public ConversationViewModel findById(Long userId, Long conversationId, String sort, Integer page, Integer size)
        throws UserNotParticipantInConversationException {
        final var conversation = this.conversationRepository.findByIdAndUserId(userId, conversationId)
            .orElseThrow(UserNotParticipantInConversationException::new);

        final var messages = this.messageRepository.findAllByConversationId(
            conversationId,
            PageRequest.of(page, size, Sort.by(Sort.Direction.DESC, sort))
        );

        return ConversationViewModel.ofConversation(conversation, messages);
    }

    @Override
    public MessageInfoViewModel createMessage(Long conversationId, MessageRequestModel model, Long userId) throws UserNotParticipantInConversationException {
        final var conversation = this.conversationRepository.findByIdAndUserId(userId, conversationId)
            .orElseThrow(UserNotParticipantInConversationException::new);

        final var message = this.setMessage(conversation, model.getMessage(), this.userRepository.findById(userId).get());

        return MessageInfoViewModel.ofMessage(
            this.messageRepository.saveAndFlush(message)
        );
    }

    @Override
    public ConversationInfoViewModel createConversationName(Long conversationId, ConversationNameRequestModel model, Long userId)
        throws UserNotParticipantInConversationException {
        final var conversation = this.conversationRepository.findByIdAndUserId(userId, conversationId)
            .orElseThrow(UserNotParticipantInConversationException::new);
        conversation.setName(model.getName());

        return ConversationInfoViewModel.ofConversation(
            this.conversationRepository.saveAndFlush(conversation)
        );
    }

    @Override
    public List<MessageInfoViewModel> findAllUnreadMessageByUserId(Long id) {
        return this.conversationRepository.findAllUnreadMessagesByUserId(id).stream()
            .map(MessageInfoViewModel::ofMessage)
            .collect(Collectors.toList());
    }

    @Override
    public List<MessageInfoViewModel> changeMessageSeenStatus(Long userId, SeenMessagesRequestModel model) {
        return this.conversationRepository
            .findAllMessagesByIdAndUserId(model.getMessageIds(), userId).stream()
            .map(Message::see)
            .map(this.messageRepository::saveAndFlush)
            .map(MessageInfoViewModel::ofMessage)
            .collect(Collectors.toList());
    }

    @Override
    public SuccessfulRequestViewModel deleteMessageById(Long id, DeleteMessageRequestModel model) throws UserCannotDeleteMessageException {
        final var message = this.messageRepository.findByIdAndUserId(model.getId(), id)
            .orElseThrow(UserCannotDeleteMessageException::new);

        this.messageRepository.deleteById(message.getId());

        return SuccessfulRequestViewModel.builder().isSuccessful(Boolean.TRUE).build();
    }

    @Override
    public ConversationInfoViewModel createConversationWithMessage(Long userId, ConversationMessageRequestModel model) throws UserNotFound {
        var friend = this.userRepository.findById(model.getFriendId()).orElseThrow(UserNotFound::new);
        final var user = this.userRepository.findById(userId).orElseThrow(UserNotFound::new);
        var conversation = this.conversationRepository.findByUsersId(userId, model.getFriendId());

        if (conversation == null) {
            conversation = new Conversation();
            conversation.addUser(user);
            conversation.addUser(friend);
            conversation.setStartedAt(java.sql.Date.from(LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant()));
            conversation = this.conversationRepository.saveAndFlush(conversation);
        }

        this.messageRepository.saveAndFlush(this.setMessage(conversation, model.getMessage(), user));

        return ConversationInfoViewModel.ofConversation(conversation);
    }

    private Message setMessage(Conversation conversation, String message, User user) {
        var m = new Message();

        m.setMessage(message);
        m.setCreatedAt(java.sql.Date.from(LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant()));
        m.setConversation(conversation);
        m.setSender(user);

        conversation.addMessage(m);
        conversation.setLastMessage(m);

        return m;
    }

}
