package com.brute.api.service.chat;

import com.brute.api.exception.user.*;
import com.brute.api.payload.SuccessfulRequestViewModel;
import com.brute.api.payload.chat.*;
import java.util.List;

public interface ChatService {

    FriendsMessagesViewModel getAllConversationsWithLastMessageByUserId(Long id, Integer page, Integer size);

    ConversationInfoViewModel createConversation(ConversationRequestModel model);

    ConversationViewModel findById(Long userId, Long conversationId, String sort, Integer page, Integer size) throws UserNotParticipantInConversationException;

    MessageInfoViewModel createMessage(Long conversationId, MessageRequestModel model, Long userId) throws UserNotParticipantInConversationException;

    ConversationInfoViewModel createConversationName(Long id, ConversationNameRequestModel model, Long userId) throws UserNotParticipantInConversationException;

    List<MessageInfoViewModel> findAllUnreadMessageByUserId(Long id);

    List<MessageInfoViewModel> changeMessageSeenStatus(Long userId, SeenMessagesRequestModel model);

    SuccessfulRequestViewModel deleteMessageById(Long id, DeleteMessageRequestModel model) throws UserCannotDeleteMessageException;

    ConversationInfoViewModel createConversationWithMessage(Long userId, ConversationMessageRequestModel model) throws UserNotFound;

}
