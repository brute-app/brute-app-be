package com.brute.api.service.upload;

import com.brute.api.entity.User;
import com.brute.api.exception.file.PictureNotFoundException;
import com.brute.api.exception.user.*;
import com.brute.api.payload.file.*;
import com.brute.api.payload.user.*;
import java.io.IOException;


public interface UploadService {

    UploadViewModel uploadFile(UploadRequestModel request, User user) throws IOException, UserNotFound;

    UserInfoViewModel setProfilePicture(UserProfileUploadRequestModel request, Long id) throws UserNotFound, PictureNotFoundException;

    UserUploadsViewModel findAllUploadsByUserIdAndTypeId(Long userId, Long typeId, String sort, Integer page, Integer size);

    UploadViewModel changeUploadType(UploadChangeTypeRequestModel request, Long id) throws UserNotAuthorized;

    UploadViewModel deleteUpload(UserProfileUploadRequestModel request, Long id) throws UserNotAuthorized;

}
