package com.brute.api.service.horoscope;

import com.brute.api.entity.*;
import com.brute.api.exception.horoscope.HoroscopeNotFound;
import com.brute.api.payload.SuccessfulRequestViewModel;
import com.brute.api.payload.horoscope.*;
import com.brute.api.repository.HoroscopeRepository;
import java.util.List;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;

@Service
public class HoroscopeServiceImpl implements HoroscopeService {

    private final HoroscopeRepository horoscopeRepository;

    public HoroscopeServiceImpl(HoroscopeRepository horoscopeRepository) {
        this.horoscopeRepository = horoscopeRepository;
    }

    @Override
    public Page<HoroscopeModel> getAll(Pageable pageable) {
        return horoscopeRepository.findAll(pageable)
            .map(HoroscopeModel::ofHoroscope);
    }

    @Override
    public List<ZodiacSign> getAllZodiacSigns() {
        return List.of(ZodiacSign.values());
    }

    @Override
    public HoroscopeModel getWeeklyByZodiacSign(String text) {
        ZodiacSign zodiacSign = ZodiacSign.from(text);
        List<Horoscope> allByZodiacSign = horoscopeRepository.findAllByZodiacSign(zodiacSign);
        return HoroscopeModel.ofHoroscope(allByZodiacSign.stream().findFirst().orElse(null));
    }

    @Override
    public HoroscopeModel add(HoroscopeRequestModel model) {
        Horoscope horoscope = new Horoscope();
        horoscope.setSign(model.getSign());
        horoscope.setInfo(model.getInfo());
        horoscope.setPeriodFrom(model.getFrom());
        horoscope.setPeriodTo(model.getTo());

        return HoroscopeModel.ofHoroscope(
            horoscopeRepository.save(horoscope)
        );
    }

    @Override
    public HoroscopeModel edit(HoroscopeRequestModel model) {
        Horoscope horoscope = horoscopeRepository.findById(model.getId())
            .orElseThrow(HoroscopeNotFound::new);

        horoscope.setSign(model.getSign());
        horoscope.setInfo(model.getInfo());
        horoscope.setPeriodFrom(model.getFrom());
        horoscope.setPeriodTo(model.getTo());

        return HoroscopeModel.ofHoroscope(
            horoscopeRepository.save(horoscope)
        );
    }

    @Override
    public SuccessfulRequestViewModel delete(Long id) {
        try {
            horoscopeRepository.deleteById(id);
        } catch (Exception e) {
            return SuccessfulRequestViewModel.builder()
                .isSuccessful(false)
                .message(e.getMessage())
                .build();
        }
        return SuccessfulRequestViewModel.builder()
            .isSuccessful(false)
            .build();
    }

}
