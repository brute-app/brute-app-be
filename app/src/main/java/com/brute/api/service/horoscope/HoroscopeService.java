package com.brute.api.service.horoscope;

import com.brute.api.entity.ZodiacSign;
import com.brute.api.payload.SuccessfulRequestViewModel;
import com.brute.api.payload.horoscope.*;
import java.util.List;
import org.springframework.data.domain.*;

public interface HoroscopeService {

    Page<HoroscopeModel> getAll(Pageable pageable);

    List<ZodiacSign> getAllZodiacSigns();

    HoroscopeModel getWeeklyByZodiacSign(String zodiacSign);

    HoroscopeModel add(HoroscopeRequestModel model);

    HoroscopeModel edit(HoroscopeRequestModel model);

    SuccessfulRequestViewModel delete(Long id);
}
