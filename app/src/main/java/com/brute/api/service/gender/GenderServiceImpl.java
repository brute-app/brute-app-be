package com.brute.api.service.gender;

import com.brute.api.payload.gender.GenderInfoViewModel;
import com.brute.api.repository.GenderRepository;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class GenderServiceImpl implements GenderService {

    private final GenderRepository genderRepository;

    public GenderServiceImpl(GenderRepository genderRepository) {
        this.genderRepository = genderRepository;
    }

    @Override
    public List<GenderInfoViewModel> getAll() {
        return this.genderRepository.findAllAndSortById()
            .stream()
            .map(GenderInfoViewModel::ofGender)
            .collect(Collectors.toList());
    }

}
