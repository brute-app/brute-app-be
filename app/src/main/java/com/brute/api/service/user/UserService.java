package com.brute.api.service.user;

import com.brute.api.entity.User;
import com.brute.api.exception.user.*;
import com.brute.api.payload.SuccessfulRequestViewModel;
import com.brute.api.payload.user.*;
import java.util.List;
import org.springframework.data.domain.*;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends UserDetailsService {

    User getPrincipal();

    Page<UserInfoViewModel> findAll(Pageable pageable);

    UserInfoViewModel register(UserRegisterRequestModel request) throws PasswordMismatchException, DuplicateEmailException, DuplicateUsernameException;

    UserInfoViewModel getProfile(Long id) throws UserNotFound;

    UserInfoViewModel changeUsername(Long id, UsernameRequestModel model) throws UserNotFound, DuplicateUsernameException;

    UserInfoViewModel changePassword(Long id, UserPasswordRequestModel model) throws PasswordMismatchException, UserNotFound;

    SuccessfulRequestViewModel delete(Long id);

    UserInfoViewModel changeEmail(Long id, EmailRequestModel model);

    List<UserInfoViewModel> getMyReferrals(Long id);

    UserBalanceModel getMyBalance(Long id);

}
