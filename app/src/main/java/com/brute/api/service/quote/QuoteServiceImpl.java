package com.brute.api.service.quote;

import com.brute.api.entity.Quote;
import com.brute.api.exception.ObjectNotFoundException;
import com.brute.api.exception.file.FileException;
import com.brute.api.exception.quote.QuoteNotFound;
import com.brute.api.payload.quote.*;
import com.brute.api.repository.QuoteRepository;
import java.sql.Date;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;
import org.slf4j.*;
import org.springframework.data.domain.*;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class QuoteServiceImpl implements QuoteService {

    private static final Logger LOGGER = LoggerFactory.getLogger(QuoteServiceImpl.class);
    private static final Random random = new Random();
    private final QuoteRepository quoteRepository;
    private final QuoteConverter quoteConverter;

    public QuoteServiceImpl(QuoteRepository quoteRepository, QuoteConverter quoteConverter) {
        this.quoteRepository = quoteRepository;
        this.quoteConverter = quoteConverter;
    }

    @Override
    public Page<QuoteModel> findAll(Pageable pageable) {
        return quoteRepository.findAll(pageable)
            .map(QuoteModel::ofQuote);
    }

    @Override
    public List<QuoteModel> readQuoteExcel(MultipartFile file) {
        if (file == null || file.isEmpty()) {
            throw new FileException();
        }
        List<Quote> quotes = quoteRepository.saveAll(quoteConverter.readExcel(file));
        return quotes.stream()
            .map(QuoteModel::ofQuote)
            .collect(Collectors.toList());
    }

    @Override
    public QuoteModel update(QuoteRequestModel model) {
        Quote quote = quoteRepository.findById(model.getId())
            .orElseThrow(QuoteNotFound::new);

        quote.setQuote(model.getQuote());
        quote.setAuthor(model.getAuthor());
        quote.setDateShown(model.getDateShown());

        return QuoteModel.ofQuote(quoteRepository.save(quote));
    }

    @Override
    public QuoteModel getDailyQuote() {
        Date today = Date.valueOf(LocalDate.now());
        Quote dailyQuote = quoteRepository.findFirstByDateShown(today)
            .orElseThrow(ObjectNotFoundException::new);
        return QuoteModel.ofQuote(dailyQuote);
    }

    //cron -> <minute> <hour> <day-of-month> <month> <day-of-week> <command>
    //fixedDelay ->  milliseconds
    //86400000 ->  24 hours
    @Scheduled(fixedDelay = 86400000)
    public void pickupDailyQuote() {
        List<Quote> quotes = quoteRepository.findAllByDateShownIsNull();
        if(quotes.isEmpty()) {
            LOGGER.info("CRON >>> there are no free quotes to pickup");
            return;
        }
        LOGGER.info("CRON >>> daily quote");
        int i = randomInt(quotes.size());
        Quote quoteForTomorrow = quotes.get(i);
        LocalDate today = LocalDate.now();
        String tomorrow = (today.plusDays(1)).format(DateTimeFormatter.ISO_DATE);
        quoteForTomorrow.setDateShown(Date.valueOf(tomorrow));
        quoteRepository.save(quoteForTomorrow);
    }

    private int randomInt(int size) {
        return random.nextInt(size);
    }
}
