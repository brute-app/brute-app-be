package com.brute.api.service.upload;

import static com.brute.api.constant.ConfigurationConstants.PictureConfig.*;

import com.brute.api.entity.*;
import com.brute.api.exception.file.PictureNotFoundException;
import com.brute.api.exception.user.*;
import com.brute.api.payload.file.*;
import com.brute.api.payload.user.*;
import com.brute.api.repository.*;
import com.brute.api.service.common.io.PictureService;
import java.io.IOException;
import java.util.Date;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;

@Service
public class UploadServiceImpl implements UploadService {

    private final UserRepository userRepository;

    private final UploadRepository uploadRepository;

    private final PictureService pictureService;

    public UploadServiceImpl(UserRepository userRepository, UploadRepository uploadRepository, PictureService pictureService) {
        this.userRepository = userRepository;
        this.uploadRepository = uploadRepository;
        this.pictureService = pictureService;
    }

    @Override
    public UserUploadsViewModel findAllUploadsByUserIdAndTypeId(Long userId, Long typeId, String sort, Integer page, Integer size) {
        final var pageable = PageRequest.of(page, size, Sort.by(Sort.Direction.DESC, sort));
        return UserUploadsViewModel.ofUserUploads(this.uploadRepository.findAllByUserAndTypeIdWithPaging(userId, typeId, pageable));
    }

    @Override
    public UploadViewModel changeUploadType(UploadChangeTypeRequestModel request, Long id) throws UserNotAuthorized {
        Upload upload = this.uploadRepository.findByIdAndUserId(request.getUploadId(), id)
            .orElseThrow(UserNotAuthorized::new);

        upload.setTypeId(request.getTypeId());

        return UploadViewModel.ofUpload(this.uploadRepository.saveAndFlush(upload));
    }

    @Override
    public UploadViewModel deleteUpload(UserProfileUploadRequestModel request, Long id) throws UserNotAuthorized {
        Upload upload = this.uploadRepository.findByIdAndUserId(request.getUploadId(), id)
            .orElseThrow(UserNotAuthorized::new);

        this.uploadRepository.delete(upload);

        return UploadViewModel.ofUpload(upload);
    }

    @Override
    public UploadViewModel uploadFile(UploadRequestModel request, User user) throws IOException, UserNotFound {
        final var user1 = this.userRepository.findFirstByEmail(user.getEmail())
            .orElseThrow(UserNotFound::new);

        var filePath = PATH_USERS + FULL_PREFIX + new Date().getTime() + "-" + user.getId();
        var images = this.pictureService.uploadImg(filePath, request.getFile());

        var upload = new Upload();
        upload.setUrl(images.get(0));
        upload.setThumbnailUrl(images.get(1));
        upload.setTypeId(request.getTypeId());
        upload.setVideo(request.getVideo());
        upload.setUser(user1);

        return UploadViewModel.ofUpload(this.uploadRepository.saveAndFlush(upload));
    }

    @Override
    public UserInfoViewModel setProfilePicture(UserProfileUploadRequestModel model, Long id) throws UserNotFound, PictureNotFoundException {
        var user = this.userRepository.findById(id)
            .orElseThrow(UserNotFound::new);
        user.setProfileImg(
            this.uploadRepository.findById(model.getUploadId())
                .orElseThrow(PictureNotFoundException::new)
        );

        return UserInfoViewModel.fromUser(
            this.userRepository.saveAndFlush(user)
        );
    }
}
