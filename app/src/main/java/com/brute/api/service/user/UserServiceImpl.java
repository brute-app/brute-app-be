package com.brute.api.service.user;

import java.security.SecureRandom;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;

import com.brute.api.entity.*;
import com.brute.api.exception.user.DuplicateEmailException;
import com.brute.api.exception.user.DuplicateUsernameException;
import com.brute.api.exception.user.PasswordMismatchException;
import com.brute.api.exception.user.UserNotFound;
import com.brute.api.payload.SuccessfulRequestViewModel;
import com.brute.api.payload.user.*;
import com.brute.api.repository.*;
import com.brute.api.service.common.io.DefaultPictureService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    static final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    static final SecureRandom RANDOM = new SecureRandom();
    static final int REFERRAL_CODE_LENGTH = 9;
    static final int REFERRAL_POINTS = 50;
    static final String REFERRAL_DESCRIPTION = "invited refferal";

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final String staticFileDestination;
    private final RoleRepository roleRepository;
    private final UploadRepository uploadRepository;
    private final GenderRepository genderRepository;
    private final ReferralRepository referralRepository;
    private final TransactionRepository transactionRepository;

    public UserServiceImpl(
        UserRepository userRepository,
        PasswordEncoder passwordEncoder,
        @Value("${static.fileDestination}") String staticFileDestination,
        RoleRepository roleRepository,
        UploadRepository uploadRepository,
        GenderRepository genderRepository,
        ReferralRepository referralRepository,
        TransactionRepository transactionRepository
    ) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.staticFileDestination = staticFileDestination;
        this.roleRepository = roleRepository;
        this.uploadRepository = uploadRepository;
        this.genderRepository = genderRepository;
        this.referralRepository = referralRepository;
        this.transactionRepository = transactionRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        return this.userRepository.findFirstByEmailOrUsername(s, s)
            .orElseThrow(() -> new UsernameNotFoundException("Username not found"));
    }

    @Override
    public User getPrincipal() {
        return (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }

    @Override
    public Page<UserInfoViewModel> findAll(Pageable pageable) {
        return userRepository.findAll(pageable).map(UserInfoViewModel::fromUser);
    }

    @Override
    public UserInfoViewModel register(UserRegisterRequestModel request)
        throws PasswordMismatchException, DuplicateEmailException, DuplicateUsernameException {
        if (!request.getPassword().equals(request.getConfirm())) {
            throw new PasswordMismatchException();
        }

        if (this.userRepository.findFirstByEmail(request.getEmail()).isPresent()) {
            throw new DuplicateEmailException();
        }

        if (this.userRepository.findFirstByUsername(request.getUsername()).isPresent()) {
            throw new DuplicateUsernameException();
        }

        Date today = java.sql.Date.from(LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant());
        String referralToken = this.getReferralToken();

        var u = new User();
        u.setDeleted(false);
        u.setUsername(request.getUsername());
        u.setEmail(request.getEmail());
        u.setPassword(this.passwordEncoder.encode(request.getPassword()));
        u.addRole(this.roleRepository.findById(1L).get());
        u.setFullName(request.getFullName());
        u.setPhoneNumber(request.getPhoneNumber());
        u.setReferralToken(referralToken);
        u.setRegisteredOn(today);
        u.setGender(this.genderRepository.findById(request.getGenderId()).get());

        final var user = this.userRepository.saveAndFlush(u);

        addPointsToReferralUser(user.getId(), request.getReferralCode());

        return UserInfoViewModel.fromUser(user);
    }

    @Override
    public UserInfoViewModel getProfile(Long id) throws UserNotFound {
        return UserInfoViewModel.fromUser(this.userRepository.findById(id).orElseThrow(UserNotFound::new));
    }

    @Override
    public UserInfoViewModel changeUsername(Long id, UsernameRequestModel model) throws UserNotFound, DuplicateUsernameException {
        if (this.userRepository.findFirstByUsername(model.getUsername()).isPresent()) {
            throw new DuplicateUsernameException();
        }

        final var user = this.userRepository.findById(id).orElseThrow(UserNotFound::new);

        user.setUsername(model.getUsername());

        return UserInfoViewModel.fromUser(this.userRepository.saveAndFlush(user));
    }

    @Override
    public UserInfoViewModel changePassword(Long id, UserPasswordRequestModel request) throws PasswordMismatchException, UserNotFound {
        if (!request.getPassword().equals(request.getConfirm())) {
            throw new PasswordMismatchException();
        }

        var user = this.userRepository.findById(id).orElseThrow(UserNotFound::new);

        user.setPassword(this.passwordEncoder.encode(request.getPassword()));

        return UserInfoViewModel.fromUser(this.userRepository.saveAndFlush(user));
    }

    private void deleteRole(User user, Role role) {
        role.getUsers().remove(user);
        user.getRoles().remove(role);
        this.roleRepository.saveAndFlush(role);
    }

    private void deleteUploads(User u, Set<Upload> uploads) {
        DefaultPictureService pictureService = new DefaultPictureService(this.staticFileDestination);
        pictureService.deleteAll(uploads);
        pictureService.deleteImg(u.getProfileImg().getUrl(), u.getProfileImg().getThumbnailUrl());

        u.setProfileImg(null);
        u.setUploads(null);

        this.uploadRepository.deleteAll(uploads);
    }

    @Override
    public SuccessfulRequestViewModel delete(Long id) {
        User user = this.userRepository.findById(id)
            .orElseThrow(UserNotFound::new);
        user.getRoles().forEach(r -> this.deleteRole(user, r));
        user.setRoles(null);

        Set<Upload> uploads = user.getUploads();
        if (!uploads.isEmpty()) {
            this.deleteUploads(user, uploads);
        }
        this.userRepository.deleteById(id);

        return SuccessfulRequestViewModel.builder().isSuccessful(Boolean.TRUE).build();
    }

    @Override
    public UserInfoViewModel changeEmail(Long id, EmailRequestModel model) {
        if (this.userRepository.findFirstByEmail(model.getEmail()).isPresent()) {
            throw new DuplicateEmailException();
        }

        User user = this.userRepository.findById(id)
            .orElseThrow(UserNotFound::new);
        user.setEmail(model.getEmail());

        return UserInfoViewModel.fromUser(this.userRepository.saveAndFlush(user));
    }

    @Override
    public List<UserInfoViewModel> getMyReferrals(Long id) {
        List<Referral> referrals = referralRepository.findAllByReferralUser(id);
        List<Long> userIds = referrals.stream().map(Referral::getUserId).collect(Collectors.toList());
        List<User> users = userRepository.findAllById(userIds);
        return users.stream()
            .map(UserInfoViewModel::fromUser)
            .collect(Collectors.toList());
    }

    @Override
    public UserBalanceModel getMyBalance(Long id) {
        Long balance = this.transactionRepository.getPointsByUserTransactions(id);

        return UserBalanceModel.builder().balance(balance == null ? 0 : balance).build();
    }

    @SuppressWarnings("unused")
    private int getAge(Date b, Date t) {
        Calendar birthdate = getCalendar(b);
        Calendar today = getCalendar(t);
        int age = today.get(Calendar.YEAR) - birthdate.get(Calendar.YEAR);
        if (birthdate.get(Calendar.MONTH) > today.get(Calendar.MONTH) ||
            (birthdate.get(Calendar.MONTH) == today.get(Calendar.MONTH) &&
                birthdate.get(Calendar.DATE) > today.get(Calendar.DATE))
        ) {
            age--;
        }

        return age;
    }

    private Calendar getCalendar(Date date) {
        Calendar cal = Calendar.getInstance(Locale.US);
        cal.setTime(date);

        return cal;
    }

    private String getReferralToken() {
        int length = AB.length();
        StringBuilder sb = new StringBuilder(REFERRAL_CODE_LENGTH);
        for (int i = 0; i < REFERRAL_CODE_LENGTH; i++) {
            sb.append(AB.charAt(RANDOM.nextInt(length)));
        }
        return sb.toString();
    }

    private void addPointsToReferralUser(Long userId, String referralCode) {
        if (referralCode != null && !referralCode.isEmpty()) {
            Optional<User> optUserByReferralToken = userRepository.findFirstByReferralToken(referralCode);
            if (optUserByReferralToken.isPresent()) {
                User userByReferralToken = optUserByReferralToken.get();
                Referral referral = new Referral();
                referral.setUserId(userId);
                referral.setReferralUser(userByReferralToken.getId());

                Transaction transaction = new Transaction();
                transaction.setUser(userByReferralToken);
                transaction.setPoints(REFERRAL_POINTS);
                transaction.setDescription(REFERRAL_DESCRIPTION);

                transactionRepository.saveAndFlush(transaction);
                referralRepository.saveAndFlush(referral);
            }
        }
    }

//    // https://cron.help/
//    @Scheduled(cron = "0 5 * * *")
//    private void addBonusPointsEveryDay() {
//        final int bonusPoints = 20;
//        final String description = "Daily bonus points";
//        List<User> users = userRepository.findAll();
//        users.forEach(user -> user.addPoints(bonusPoints));
//        userRepository.saveAll(users);
//
//        List<Transaction> transactions = users.stream()
//            .map(user -> {
//                Transaction transaction = new Transaction();
//                transaction.setUser(user);
//                transaction.setPoints(bonusPoints);
//                transaction.setDescription(description);
//                return transaction;
//            })
//            .collect(Collectors.toList());
//
//        transactionRepository.saveAll(transactions);
//    }
}
