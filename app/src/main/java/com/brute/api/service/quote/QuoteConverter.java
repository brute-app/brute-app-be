package com.brute.api.service.quote;

import com.brute.api.entity.Quote;
import com.brute.api.exception.file.FileException;
import java.io.InputStream;
import java.util.*;
import org.apache.poi.openxml4j.util.ZipSecureFile;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.*;
import org.slf4j.*;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class QuoteConverter {

    private static final Logger LOGGER = LoggerFactory.getLogger(QuoteConverter.class);
    private static final String QUOTE_EXCEL = "static\\quote.xlsx";
    private static final String QUOTE_MESSAGE = "Updating Quotes from excel";

    public List<Quote> readExcel(MultipartFile file) {
        XSSFWorkbook workbook;
        try {
            LOGGER.info(QUOTE_MESSAGE);
            ZipSecureFile.setMinInflateRatio(-1.0d);
            ClassPathResource excelResource = new ClassPathResource(QUOTE_EXCEL);
            file.transferTo(excelResource.getFile());
            InputStream inputStream = excelResource.getInputStream();
            workbook = new XSSFWorkbook(inputStream);
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw new FileException();
        }

        return extractQuotes(workbook.getSheetAt(0));
    }

    private List<Quote> extractQuotes(XSSFSheet excel) {
        List<Quote> quotes = new ArrayList<>();
        int skipRow = 0;
        for (Row row : excel) {
            if (skipRow++ == 0) {
                continue;
            }
            if (checkForExistingCell(row)) {
                break;
            }
            Iterator<Cell> cellIterator = row.cellIterator();
            Quote quote = new Quote();
            quote.setQuote(cellIterator.next().getStringCellValue());
            quote.setAuthor(cellIterator.next().getStringCellValue());
            quotes.add(quote);
        }
        return quotes;
    }

    /**
     * Wrong formatting of cells check for first and second cell , because sometimes they are empty and this leads to errors
     */
    private static Boolean checkForExistingCell(Row row) {
        return row.getCell(0, Row.MissingCellPolicy.RETURN_NULL_AND_BLANK) == null
            || row.getCell(1, Row.MissingCellPolicy.RETURN_NULL_AND_BLANK) == null;
    }
}
