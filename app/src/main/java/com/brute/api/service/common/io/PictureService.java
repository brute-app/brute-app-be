package com.brute.api.service.common.io;

import com.brute.api.entity.Upload;
import java.io.IOException;
import java.util.*;
import org.springframework.web.multipart.MultipartFile;

public interface PictureService {

    List<String> uploadImg(String newFilepath, MultipartFile file) throws IOException;

    boolean deleteAll(Collection<Upload> userImgs);

    boolean deleteImg(String full, String thumbnail);

}
