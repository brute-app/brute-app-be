package com.brute.api.service.gender;

import com.brute.api.payload.gender.GenderInfoViewModel;
import java.util.List;

public interface GenderService {

    List<GenderInfoViewModel> getAll();

}
