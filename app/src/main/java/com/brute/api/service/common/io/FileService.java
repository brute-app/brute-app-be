package com.brute.api.service.common.io;

import java.io.IOException;
import org.springframework.web.multipart.MultipartFile;

public interface FileService {

    String upload(String newFilepath, MultipartFile file) throws IOException;

}
