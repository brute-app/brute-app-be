package com.brute.api.service.quote;

import com.brute.api.payload.quote.*;
import java.util.List;
import org.springframework.data.domain.*;
import org.springframework.web.multipart.MultipartFile;

public interface QuoteService {

    Page<QuoteModel> findAll(Pageable pageable);

    List<QuoteModel> readQuoteExcel(MultipartFile file);

    QuoteModel update(QuoteRequestModel model);

    QuoteModel getDailyQuote();

}
