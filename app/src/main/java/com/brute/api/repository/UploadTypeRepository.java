package com.brute.api.repository;

import com.brute.api.entity.UploadType;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

@Repository
public interface UploadTypeRepository extends JpaRepository<UploadType, Long> {

//    @Query("SELECT u FROM UploadType u WHERE u.name = ?1")
//    UploadType findByName(String name);
}
