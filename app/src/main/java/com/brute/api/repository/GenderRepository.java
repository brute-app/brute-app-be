package com.brute.api.repository;

import com.brute.api.entity.Gender;
import java.util.*;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

@Repository
public interface GenderRepository extends JpaRepository<Gender, Long> {

    @Query("select g from Gender g where g.id IN (?1)")
    Set<Gender> findAllById(Collection<Long> ids);

    @Query("SELECT g FROM Gender g ORDER BY g.id")
    Set<Gender> findAllAndSortById();

}
