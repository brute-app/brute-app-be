package com.brute.api.repository;

import com.brute.api.entity.Quote;
import java.sql.Date;
import java.util.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface QuoteRepository extends JpaRepository<Quote, Long> {

    List<Quote> findAllByDateShownIsNull();

    Optional<Quote> findFirstByDateShown(Date today);
}
