package com.brute.api.repository;

import com.brute.api.entity.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Long> {

    @Query("SELECT SUM(t.points) FROM Transaction t LEFT JOIN t.user u WHERE u.id = ?1")
    Long getPointsByUserTransactions(Long userId);
}
