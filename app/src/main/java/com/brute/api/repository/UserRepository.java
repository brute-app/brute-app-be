package com.brute.api.repository;

import com.brute.api.entity.User;
import java.util.*;
import org.springframework.data.domain.*;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findFirstByEmail(String email);

    Optional<User> findFirstByUsername(String username);

    Optional<User> findFirstByEmailOrUsername(String email, String username);

    Optional<User> findFirstByReferralToken(String email);

}
