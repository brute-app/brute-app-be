package com.brute.api.repository;

import com.brute.api.entity.*;
import java.util.List;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

@Repository
public interface HoroscopeRepository extends JpaRepository<Horoscope, Long> {

    @Query("SELECT h FROM Horoscope h WHERE h.sign = ?1 ORDER BY h.id DESC")
    List<Horoscope> findAllByZodiacSign(ZodiacSign zodiacSign);
}
