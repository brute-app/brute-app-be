package com.brute.api.repository;

import com.brute.api.entity.Referral;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ReferralRepository extends JpaRepository<Referral, Long> {

    List<Referral> findAllByReferralUser(long referralId);
}
