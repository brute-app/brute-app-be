package com.brute.api.repository;

import com.brute.api.entity.Upload;
import java.util.*;
import org.springframework.data.domain.*;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

@Repository
public interface UploadRepository extends JpaRepository<Upload, Long> {

    @Query("SELECT u FROM Upload u " +
        "LEFT JOIN u.user uu " +
        "WHERE u.typeId = ?2 " +
        "AND uu.id = ?1 " +
        "AND u.video = false")
    Page<Upload> findAllByUserAndTypeIdWithPaging(Long userId, Long typeId, Pageable pageable);

    @Query("SELECT u FROM Upload u " +
        "LEFT JOIN u.user uu " +
        "WHERE u.id = ?1 " +
        "AND uu.id = ?2")
    Optional<Upload> findByIdAndUserId(Long uploadId, Long userId);
}
