package com.brute.api.repository;

import com.brute.api.entity.Message;
import java.util.Optional;
import org.springframework.data.domain.*;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

@Repository
public interface MessageRepository extends JpaRepository<Message, Long> {

    @Query("SELECT m FROM Message m LEFT JOIN m.conversation c WHERE c.id = ?1")
    Page<Message> findAllByConversationId(Long id, Pageable pageable);

    @Query("SELECT m FROM Message m WHERE m.id = ?1 AND m.sender.id = ?2")
    Optional<Message> findByIdAndUserId(Long messageId, Long userId);

}
