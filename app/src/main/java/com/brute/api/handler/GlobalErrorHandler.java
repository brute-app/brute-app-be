package com.brute.api.handler;

import com.brute.api.exception.BruteException;
import com.brute.api.payload.error.*;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.*;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class GlobalErrorHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(BruteException.class)
    protected ResponseEntity<Object> handleConflict(
        RuntimeException ex,
        WebRequest request
    ) {
        var errorViewModel = new ErrorViewModel();
        errorViewModel.add(ex.getMessage());

        return this.handleExceptionInternal(
            ex,
            errorViewModel,
            new HttpHeaders(),
            HttpStatus.CONFLICT,
            request
        );
    }

    @Override
    public ResponseEntity<Object> handleMethodArgumentNotValid(
        MethodArgumentNotValidException ex,
        HttpHeaders headers,
        HttpStatus status,
        WebRequest request
    ) {
        return new ResponseEntity<>(
            new GenericError(
                HttpStatus.BAD_REQUEST.value(),
                ex.getBindingResult()
                    .getAllErrors()
                    .stream()
                    .map(DefaultMessageSourceResolvable::getDefaultMessage)
                    .collect(Collectors.toList())
            ),
            HttpStatus.BAD_REQUEST
        );
    }

    @ExceptionHandler(Throwable.class)
    public ResponseEntity<Object> handleGlobalExceptionErrors(Throwable throwable) {
        var lastException = throwable;
        while (!(lastException instanceof BruteException) && lastException.getCause() != null) {
            lastException = lastException.getCause();
        }

        if (lastException instanceof BruteException) {
            return new ResponseEntity<>(
                new GenericError(
                    HttpStatus.BAD_REQUEST.value(),
                    lastException.getMessage() == null || lastException.getMessage().isEmpty()
                        ? List.of(lastException.getClass().getSimpleName())
                        : List.of(lastException.getMessage())
                ),
                new HttpHeaders(),
                HttpStatus.BAD_REQUEST
            );
        }

        if (lastException instanceof IllegalArgumentException) {
            return new ResponseEntity<>(
                new GenericError(
                    HttpStatus.BAD_REQUEST.value(),
                    List.of("AlteredRequestDetectedException")
                ),
                new HttpHeaders(),
                HttpStatus.BAD_REQUEST
            );
        }

        return new ResponseEntity<>(
            new GenericError(
                HttpStatus.INTERNAL_SERVER_ERROR.value(),
                List.of("UnrecoverableException_" + lastException.getClass().getName())
            ),
            new HttpHeaders(),
            HttpStatus.INTERNAL_SERVER_ERROR
        );
    }
}

