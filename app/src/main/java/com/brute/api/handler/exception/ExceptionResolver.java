package com.brute.api.handler.exception;

import javax.servlet.http.HttpServletResponse;
import org.springframework.http.HttpStatus;

public interface ExceptionResolver {

    void resolveRuntimeException(RuntimeException e, HttpServletResponse response, HttpStatus responseStatus);

}
