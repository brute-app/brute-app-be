package com.brute.api.controller;

import static com.brute.api.constant.Constants.DEFAULT_PAGE;
import static com.brute.api.constant.Constants.DEFAULT_SIZE;
import static com.brute.api.constant.URLMappings.*;
import javax.validation.Valid;
import java.util.List;

import com.brute.api.entity.User;
import com.brute.api.exception.user.UserCannotDeleteMessageException;
import com.brute.api.exception.user.UserNotFound;
import com.brute.api.exception.user.UserNotParticipantInConversationException;
import com.brute.api.payload.SuccessfulRequestViewModel;
import com.brute.api.payload.chat.*;
import com.brute.api.service.chat.ChatService;
import com.brute.api.service.user.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.Authorization;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@Validated
@Api(tags = {"Chat Controller"}, authorizations = {@Authorization(value = "jwtToken")})
public class ChatController {

    private final UserService userService;
    private final ChatService chatService;

    public ChatController(UserService userService, ChatService chatService) {
        this.chatService = chatService;
        this.userService = userService;
    }

    @GetMapping(value = CONVERSATION_BASE)
    @ApiOperation(value = "All conversations with last message for user ID.")
    public ResponseEntity<FriendsMessagesViewModel> getAllConversationWithLastMessageByUserId(
        @ApiParam(value = "Page number. The parameter is optional.", example = "3")
        @RequestParam(value = "page", required = false, defaultValue = DEFAULT_PAGE) final Integer page,
        @ApiParam(value = "Number of conversations per page. The parameter is optional.", example = "23")
        @RequestParam(value = "size", required = false, defaultValue = DEFAULT_SIZE) final Integer size
    ) {
        User user = userService.getPrincipal();

        return new ResponseEntity<>(this.chatService.getAllConversationsWithLastMessageByUserId(user.getId(), page, size), HttpStatus.OK);
    }

    @PostMapping(value = CONVERSATION_BASE)
    @PreAuthorize("isAuthenticated()")
    @ApiOperation(value = "Create conversation.")
    public ResponseEntity<ConversationInfoViewModel> createConversation(
        @ApiParam(value = "Model for creating conversation with user IDs.") //
        @RequestBody @Valid ConversationRequestModel model) {
        return new ResponseEntity<>(this.chatService.createConversation(model), HttpStatus.CREATED);
    }

    @PostMapping(value = CONVERSATION_BASE + MESSAGE_BASE)
    @PreAuthorize("isAuthenticated()")
    @ApiOperation(value = "Create conversation with message.")
    public ResponseEntity<ConversationInfoViewModel> createConversationWithMessage(
        @ApiParam(value = "Model with message for a user ID.") //
        @RequestBody @Valid ConversationMessageRequestModel model) throws UserNotFound {
        User user = userService.getPrincipal();

        return new ResponseEntity<>(this.chatService.createConversationWithMessage(user.getId(), model), HttpStatus.CREATED);
    }

    @GetMapping(value = MESSAGE_BASE)
    @PreAuthorize("isAuthenticated()")
    @ApiOperation(value = "Get all unread messages")
    public ResponseEntity<List<MessageInfoViewModel>> findAllUnreadMessageByUserId() {
        User user = userService.getPrincipal();

        return new ResponseEntity<>(this.chatService.findAllUnreadMessageByUserId(user.getId()), HttpStatus.OK);
    }

    @PostMapping(value = MESSAGE_BASE)
    @PreAuthorize("isAuthenticated()")
    @ApiOperation(value = "Change message status to seen")
    public ResponseEntity<List<MessageInfoViewModel>> changeMessageSeenStatus(
        @ApiParam(value = "Model with message IDs that are seen.") //
        @RequestBody @Valid SeenMessagesRequestModel model
    ) {
        User user = userService.getPrincipal();

        return new ResponseEntity<>(this.chatService.changeMessageSeenStatus(user.getId(), model), HttpStatus.CREATED);
    }

    @PostMapping(value = MESSAGE_DELETE)
    @PreAuthorize("isAuthenticated()")
    @ApiOperation(value = "Delete message by ID")
    public ResponseEntity<SuccessfulRequestViewModel> deleteMessageById(
        @ApiParam(value = "Message ID for deleting.") //
        @RequestBody @Valid DeleteMessageRequestModel model
    ) throws UserCannotDeleteMessageException {
        User user = userService.getPrincipal();

        return new ResponseEntity<>(this.chatService.deleteMessageById(user.getId(), model), HttpStatus.OK);
    }

    @GetMapping(value = CONVERSATION_BY_ID)
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<ConversationViewModel> findById(
        @ApiParam(value = "Conversation ID.") //
        @PathVariable Long id,
        @ApiParam(value = "Sorting") //
        @RequestParam(value = "sort", required = false) String sort,
        @ApiParam(value = "Page number") //
        @RequestParam(value = "page", required = false, defaultValue = DEFAULT_PAGE) Integer page,
        @ApiParam(value = "Size of the page") //
        @RequestParam(value = "size", required = false, defaultValue = DEFAULT_SIZE) Integer size
    ) throws UserNotParticipantInConversationException {
        User user = userService.getPrincipal();

        return new ResponseEntity<>(this.chatService.findById(user.getId(), id, sort, page, size), HttpStatus.OK);
    }

    @PostMapping(value = CONVERSATION_BY_ID)
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<MessageInfoViewModel> createMessage(
        @ApiParam(value = "Conversation ID.") //
        @PathVariable Long id,
        @ApiParam(value = "Message model.") //
        @RequestBody @Valid MessageRequestModel model
    ) throws UserNotParticipantInConversationException {
        User user = userService.getPrincipal();

        return new ResponseEntity<>(this.chatService.createMessage(id, model, user.getId()), HttpStatus.CREATED);
    }

    @PostMapping(value = CREATE_CONVERSATION_NAME)
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<ConversationInfoViewModel> createConversationName(
        @ApiParam(value = "Conversation ID.") //
        @PathVariable Long id,
        @ApiParam(value = "Conversation name model.") //
        @RequestBody @Valid ConversationNameRequestModel model
    ) throws UserNotParticipantInConversationException {
        User user = userService.getPrincipal();

        return new ResponseEntity<>(this.chatService.createConversationName(id, model, user.getId()), HttpStatus.CREATED);
    }


}
