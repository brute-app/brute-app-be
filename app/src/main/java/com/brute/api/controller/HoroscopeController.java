package com.brute.api.controller;

import static com.brute.api.constant.URLMappings.*;

import com.brute.api.constant.URLMappings;
import com.brute.api.entity.ZodiacSign;
import com.brute.api.payload.SuccessfulRequestViewModel;
import com.brute.api.payload.horoscope.*;
import com.brute.api.service.horoscope.HoroscopeService;
import java.util.List;

import io.swagger.annotations.Api;
import io.swagger.annotations.Authorization;
import org.springframework.data.domain.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(HOROSCOPE_BASE)
@Api(tags = {"Horoscope Controller"}, authorizations = { @Authorization(value="jwtToken") })
public class HoroscopeController {

    private final HoroscopeService horoscopeService;

    public HoroscopeController(HoroscopeService horoscopeService) {
        this.horoscopeService = horoscopeService;
    }

    @GetMapping
    public ResponseEntity<Page<HoroscopeModel>> getAll(Pageable pageable) {
        return ResponseEntity.ok(horoscopeService.getAll(pageable));
    }

    @GetMapping(ZODIAC_SIGNS)
    public ResponseEntity<List<ZodiacSign>> getAllZodiacs() {
        return ResponseEntity.ok(horoscopeService.getAllZodiacSigns());
    }

    @GetMapping(WEEKLY)
    public ResponseEntity<HoroscopeModel> getWeeklyByZodiacSignId(@RequestParam("sign") String zodiacSign) {
        return ResponseEntity.ok(horoscopeService.getWeeklyByZodiacSign(zodiacSign));
    }

    @PostMapping
    public ResponseEntity<HoroscopeModel> add(@RequestBody HoroscopeRequestModel model) {
        return ResponseEntity.ok(horoscopeService.add(model));
    }

    @PutMapping
    public ResponseEntity<HoroscopeModel> edit(@RequestBody HoroscopeRequestModel model) {
        return ResponseEntity.ok(horoscopeService.edit(model));
    }

    @PutMapping(value = URLMappings.PathVar.ID)
    public ResponseEntity<SuccessfulRequestViewModel> delete(@PathVariable(value = "id") Long id) {
        return ResponseEntity.ok(horoscopeService.delete(id));
    }

}
