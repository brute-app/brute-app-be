package com.brute.api.controller;

import static com.brute.api.constant.URLMappings.GENDERS_BASE;

import com.brute.api.payload.gender.GenderInfoViewModel;
import com.brute.api.service.gender.GenderService;
import java.util.List;

import io.swagger.annotations.Api;
import io.swagger.annotations.Authorization;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

@RestController
@Api(tags = {"Gender Controller"}, authorizations = { @Authorization(value="jwtToken") })
public class GenderController {

    private final GenderService genderService;

    public GenderController(GenderService genderService) {
        this.genderService = genderService;
    }

    @GetMapping(value = GENDERS_BASE)
    public ResponseEntity<List<GenderInfoViewModel>> getAll() {
        return new ResponseEntity<>(this.genderService.getAll(), HttpStatus.OK);
    }

}
