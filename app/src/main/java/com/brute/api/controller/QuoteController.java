package com.brute.api.controller;

import static com.brute.api.constant.URLMappings.QUOTE_BASE;

import com.brute.api.payload.quote.*;
import com.brute.api.service.quote.QuoteService;
import java.util.List;

import io.swagger.annotations.Api;
import io.swagger.annotations.Authorization;
import org.springframework.data.domain.*;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping(QUOTE_BASE)
@Api(tags = {"Quote Controller"}, authorizations = { @Authorization(value="jwtToken") })
public class QuoteController {

    private final QuoteService quoteService;

    public QuoteController(QuoteService quoteService) {
        this.quoteService = quoteService;
    }

    @GetMapping
    public ResponseEntity<Page<QuoteModel>> getAll(Pageable pageable) {
        return ResponseEntity.ok(quoteService.findAll(pageable));
    }

    @GetMapping(value = "/daily")
    public ResponseEntity<QuoteModel> getDailyQuote() {
        return ResponseEntity.ok(quoteService.getDailyQuote());
    }

    @PostMapping
    public ResponseEntity<List<QuoteModel>> readQuoteExcel(@RequestBody MultipartFile file) {
        return new ResponseEntity<>(quoteService.readQuoteExcel(file), HttpStatus.CREATED);
    }

    @PutMapping
    public ResponseEntity<QuoteModel> update(@RequestBody QuoteRequestModel model) {
        return new ResponseEntity<>(quoteService.update(model), HttpStatus.OK);
    }

}
