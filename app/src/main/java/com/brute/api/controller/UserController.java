package com.brute.api.controller;

import static com.brute.api.constant.URLMappings.*;
import static com.brute.api.constant.URLMappings.Common.DELETE;
import static com.brute.api.constant.URLMappings.User.*;
import java.util.List;

import com.brute.api.entity.User;
import com.brute.api.exception.user.DuplicateEmailException;
import com.brute.api.exception.user.DuplicateUsernameException;
import com.brute.api.exception.user.PasswordMismatchException;
import com.brute.api.exception.user.UserNotFound;
import com.brute.api.payload.SuccessfulRequestViewModel;
import com.brute.api.payload.user.*;
import com.brute.api.service.user.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(USER_BASE)
@Api(tags = {"User Controller"}, authorizations = {@Authorization(value = "jwtToken")})
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Retrieves information for all users.", //
        responseContainer = "List")
    public ResponseEntity<Page<UserInfoViewModel>> getAllUsers(
        Pageable pageable
    ) {
        return ResponseEntity.ok(this.userService.findAll(pageable));
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Retrieves information for all users.", //
        responseContainer = "UserInfoViewModel")
    public ResponseEntity<UserInfoViewModel> register(@RequestBody UserRegisterRequestModel model)
        throws PasswordMismatchException, DuplicateEmailException, DuplicateUsernameException {
        return new ResponseEntity<>(this.userService.register(model), HttpStatus.CREATED);
    }

    @GetMapping(value = ME_BASE)
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<UserInfoViewModel> profile() throws UserNotFound {
        User user = userService.getPrincipal();
        return ResponseEntity.ok(this.userService.getProfile(user.getId()));
    }

    @PostMapping(value = USERNAME)
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<UserInfoViewModel> changeUsername(
        @RequestBody UsernameRequestModel model
    ) throws UserNotFound, DuplicateUsernameException {
        User user = userService.getPrincipal();

        return new ResponseEntity<>(this.userService.changeUsername(user.getId(), model), HttpStatus.CREATED);
    }

    @PostMapping(value = PASSWORD)
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<UserInfoViewModel> changePassword(
        @RequestBody UserPasswordRequestModel model
    ) throws PasswordMismatchException, UserNotFound {
        User user = userService.getPrincipal();

        return new ResponseEntity<>(this.userService.changePassword(user.getId(), model), HttpStatus.CREATED);
    }

    @PostMapping(value = EMAIL)
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<UserInfoViewModel> changeEmail(
        @RequestBody EmailRequestModel model
    ) throws UserNotFound {
        User user = userService.getPrincipal();

        return new ResponseEntity<>(this.userService.changeEmail(user.getId(), model), HttpStatus.CREATED);
    }

    @PostMapping(value = DELETE)
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<SuccessfulRequestViewModel> delete() {
        User user = userService.getPrincipal();

        return ResponseEntity.ok(this.userService.delete(user.getId()));
    }

    @GetMapping(value = REFERRALS)
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<List<UserInfoViewModel>> getMyReferrals() {
        User user = userService.getPrincipal();

        return ResponseEntity.ok(this.userService.getMyReferrals(1L));
    }

    @GetMapping(value = BALANCE)
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<UserBalanceModel> getMyBalance() {
        User user = userService.getPrincipal();

        return ResponseEntity.ok(this.userService.getMyBalance(user.getId()));
    }
}
