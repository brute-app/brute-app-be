package com.brute.api.constant;

public class Constants {

    public static final String ROLE_ADMIN = "ADMIN";

    public static final String ROLE_USER = "USER";

    public static final String ROLE_MODERATOR = "MODERATOR";

    public static final String ROLE_HOROSCOPE = "HOROSCOPE";

    public static final String[] ALL_ROLES = new String[]{ROLE_ADMIN, ROLE_USER, ROLE_MODERATOR, ROLE_HOROSCOPE};

    public static final String DEFAULT_PAGE = "0";

    public static final String DEFAULT_SIZE = "23";
}
