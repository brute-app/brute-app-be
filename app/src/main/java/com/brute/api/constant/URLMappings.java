package com.brute.api.constant;

import static com.brute.api.constant.URLMappings.Common.DELETE;
import static com.brute.api.constant.URLMappings.PathVar.ID;

public final class URLMappings {

    public static final String ALL_MAPPINGS = "/**";
    public static final String USER_BASE = "/users";
    public static final String ME_BASE = "/me";
    public static final String CONVERSATION_BASE = "/conversations";
    public static final String GENDERS_BASE = "/genders";
    public static final String MESSAGE_BASE = "/messages";
    public static final String QUOTE_BASE = "/quotes";
    public static final String HOROSCOPE_BASE = "/horoscopes";

    public static final String WEEKLY = "/weekly";
    public static final String ZODIAC_SIGNS = "/signs";
    public static final String REFERRALS = "/referrals";
    public static final String BALANCE = "/balance";

    public static final String MESSAGE_DELETE = MESSAGE_BASE + DELETE;
    public static final String CONVERSATION_BY_ID = CONVERSATION_BASE + ID;
    public static final String CREATE_CONVERSATION_NAME = CONVERSATION_BASE + ID + URLMappings.Common.NAME;

    public static final class User {

        public static final String PASSWORD = "/edit/passwords";
        public static final String USERNAME = "/edit/usernames";
        public static final String EMAIL = "/edit/email";
    }

    public static final class Common {

        public static final String DELETE = "/delete";
        public static final String NAME = "/name";
    }

    public static final class PathVar {

        public static final String ID = "/{id}";
    }
}
