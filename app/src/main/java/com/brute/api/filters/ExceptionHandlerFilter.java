package com.brute.api.filters;

import com.brute.api.handler.exception.ExceptionResolver;
import java.io.IOException;
import javax.servlet.*;
import javax.servlet.http.*;
import org.springframework.http.HttpStatus;
import org.springframework.web.filter.OncePerRequestFilter;

public class ExceptionHandlerFilter extends OncePerRequestFilter {

    private final ExceptionResolver exceptionResolver;

    public ExceptionHandlerFilter(ExceptionResolver exceptionResolver) {
        this.exceptionResolver = exceptionResolver;
    }

    @Override
    public void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        try {
            filterChain.doFilter(request, response);
        } catch (RuntimeException ext) {
            this.exceptionResolver.resolveRuntimeException(ext, response, HttpStatus.BAD_REQUEST);
        }
    }


}
