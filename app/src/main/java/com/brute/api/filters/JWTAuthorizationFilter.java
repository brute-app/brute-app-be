package com.brute.api.filters;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.brute.api.constant.ConfigurationConstants;
import java.io.IOException;
import javax.servlet.*;
import javax.servlet.http.*;
import org.springframework.security.authentication.*;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

public class JWTAuthorizationFilter extends BasicAuthenticationFilter {

    private final String secretKey;

    private final UserDetailsService userDetailsService;

    public JWTAuthorizationFilter(AuthenticationManager authenticationManager,
        String secretKey,
        UserDetailsService userDetailsService) {
        super(authenticationManager);
        this.secretKey = secretKey;
        this.userDetailsService = userDetailsService;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest req,
        HttpServletResponse res,
        FilterChain chain
    ) throws IOException, ServletException {
        var header = req.getHeader(ConfigurationConstants.HEADER_AUTHORIZATION);

        if (header == null || !header.startsWith(ConfigurationConstants.TOKEN_PREFIX)) {
            chain.doFilter(req, res);
            return;
        }

        var authentication = getAuthentication(req);

        SecurityContextHolder.getContext().setAuthentication(authentication);
        chain.doFilter(req, res);
    }

    private UsernamePasswordAuthenticationToken getAuthentication(HttpServletRequest request) {
        var token = request.getHeader(ConfigurationConstants.HEADER_AUTHORIZATION);
        if (token != null) {
            String user = JWT.require(Algorithm.HMAC512(this.secretKey.getBytes()))
                .build()
                .verify(token.replace(ConfigurationConstants.TOKEN_PREFIX, ""))
                .getSubject();

            if (user != null) {
                var entity = this.userDetailsService.loadUserByUsername(user);
                return new UsernamePasswordAuthenticationToken(
                    entity,
                    null,
                    entity.getAuthorities()
                );
            }

            return null;
        }

        return null;
    }
}
