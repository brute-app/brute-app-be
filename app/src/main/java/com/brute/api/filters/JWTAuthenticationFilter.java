package com.brute.api.filters;

import static com.auth0.jwt.algorithms.Algorithm.HMAC512;
import static com.brute.api.constant.ConfigurationConstants.Headers.*;
import static com.brute.api.constant.ConfigurationConstants.*;

import com.auth0.jwt.JWT;
import com.auth0.jwt.impl.PublicClaims;
import com.brute.api.constant.ConfigurationConstants;
import com.brute.api.entity.User;
import com.brute.api.payload.user.UserLoginRequestModel;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.net.*;
import java.util.*;
import java.util.stream.Collectors;
import javax.servlet.FilterChain;
import javax.servlet.http.*;
import org.springframework.security.authentication.*;
import org.springframework.security.core.*;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;


public class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    private final ObjectMapper objectMapper;

    private final AuthenticationManager authenticationManager;

    private final String secretKey;

    private final UserDetailsService userDetailsService;

    private final InetAddress inetAddress;

    public JWTAuthenticationFilter(ObjectMapper objectMapper,
        AuthenticationManager authenticationManager,
        String secretKey,
        UserDetailsService userDetailsService) throws UnknownHostException {
        this.objectMapper = objectMapper;
        this.authenticationManager = authenticationManager;
        this.secretKey = secretKey;
        this.userDetailsService = userDetailsService;
        this.inetAddress = InetAddress.getLocalHost();
    }

    @Override
    public Authentication attemptAuthentication(
        HttpServletRequest req,
        HttpServletResponse res
    ) throws AuthenticationException {
        try {
            var inputCredentials = this.objectMapper.readValue(
                req.getInputStream(),
                UserLoginRequestModel.class
            );

            var user = this.userDetailsService.loadUserByUsername(inputCredentials.getUsername());
            res.addHeader(ACCESS_NAME, ACCESS_VALUE);
            return authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                    user,
                    inputCredentials.getPassword(),
                    user.getAuthorities()
                )
            );
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    protected void successfulAuthentication(
        HttpServletRequest req,
        HttpServletResponse res,
        FilterChain chain,
        Authentication auth
    ) throws IOException {
        var userEntity = ((User) auth.getPrincipal());

        var token = JWT.create()
            .withIssuedAt(new Date())
            .withSubject(userEntity.getEmail())
            .withAudience(userEntity.getId().toString())
            .withJWTId(UUID.randomUUID().toString())
            .withKeyId(userEntity.getId().toString())
            .withClaim(ROLES, userEntity.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.joining(",")))
            .withIssuer(this.inetAddress.getCanonicalHostName())
            .withExpiresAt(
                new Date(System.currentTimeMillis() + ConfigurationConstants.EXPIRATION_TIME)
            )
            .sign(HMAC512(this.secretKey.getBytes()));

        res.addHeader(ConfigurationConstants.HEADER_AUTHORIZATION, ConfigurationConstants.TOKEN_PREFIX + token);

        res.setContentType(PublicClaims.CONTENT_TYPE);
        res.setCharacterEncoding(UTF);
        res.getWriter().write(this.objectMapper.writeValueAsString(Map.of(LOGIN_USER_ID, userEntity.getId())));
    }
}
