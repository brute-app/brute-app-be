package com.brute;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class BruteBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(BruteBackendApplication.class, args);
    }

}
